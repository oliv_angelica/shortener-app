package com.nubank.injection

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.support.v7.app.AppCompatActivity
import com.nubank.data.source.ShortenerRepository
import com.nubank.data.source.local.AppDatabase
import com.nubank.data.source.local.ShortenerDao
import com.nubank.data.source.remote.ShortenerAPI
import com.nubank.data.source.remote.ShortenerRemoteDataSourceImpl
import com.nubank.util.NetworkCheck
import com.nubank.view.shortener.ShortenerViewModelFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object Injection {

    inline fun <reified T : ViewModel> provideShortenerViewModel(
            activity: AppCompatActivity,
            factoryParam: ViewModelProvider.Factory?) : T {
        val factory = factoryParam ?: provideShortenerViewModelFactory(activity)
        return ViewModelProviders.of(activity, factory).get(T::class.java)
    }

    fun provideShortenerViewModelFactory(activity: AppCompatActivity) = ShortenerViewModelFactory(Injection.provideSortenerRepository(activity))

    fun provideSortenerRepository(context: Context) = ShortenerRepository(provideRemoteDataSource(context),
            provideShortenerDao(context))

    fun provideRemoteDataSource(context: Context) = ShortenerRemoteDataSourceImpl(provideShortnerAPI(), NetworkCheck(context))

    fun provideShortnerAPI() : ShortenerAPI {
        val retrofit = Retrofit.Builder()
                .baseUrl(ShortenerAPI.baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        return retrofit.create<ShortenerAPI>(ShortenerAPI::class.java)
    }

    fun provideShortenerDao(context: Context) : ShortenerDao =
            AppDatabase.getInstance(context)
                    .shortenerDao()
}