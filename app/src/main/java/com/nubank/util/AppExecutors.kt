package com.nubank.util

import java.util.concurrent.Executors

object AppExecutors {
    private val IO_EXECUTOR = Executors.newSingleThreadExecutor()

    /**
     * Utility method to run blocks on a dedicated background thread, used for io/database work.
     */
    fun runOnIoThread(f: () -> Unit) {
        IO_EXECUTOR.execute(f)
    }
}