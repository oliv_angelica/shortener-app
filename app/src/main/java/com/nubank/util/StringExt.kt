package com.nubank.util

import android.util.Patterns
import android.webkit.URLUtil

/**
 * Extension functions for String class
 */

/**
 * Check if String is a URL
 */
fun String.isURL() = Patterns.WEB_URL.matcher(this).matches()