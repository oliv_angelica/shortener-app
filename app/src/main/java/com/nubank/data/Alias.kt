package com.nubank.data

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "alias")
data class Alias(@PrimaryKey val alias: String)