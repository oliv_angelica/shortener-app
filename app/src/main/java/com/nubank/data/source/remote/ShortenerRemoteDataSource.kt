package com.nubank.data.source.remote

import com.nubank.data.Alias
import com.nubank.data.source.DataCallback

interface ShortenerRemoteDataSource {
    fun getShortenerResult(urlString: String, callback: DataCallback)

    fun getFullUrl(alias: Alias, callback: DataCallback)
}