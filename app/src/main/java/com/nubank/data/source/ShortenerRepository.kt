package com.nubank.data.source

import com.nubank.data.Alias
import com.nubank.data.ErrorResponse
import com.nubank.data.source.local.ShortenerDao
import com.nubank.data.source.remote.ShortenerRemoteDataSource
import com.nubank.util.AppExecutors.runOnIoThread


class ShortenerRepository(private val shortenerRemoteDataSource: ShortenerRemoteDataSource, private val shortenerLocalDataSource: ShortenerDao) {
    fun getAllAliases() = shortenerLocalDataSource.getAllAliases()

    fun requestShortenerResult(urlString: String, callback: DataCallback) {
        shortenerRemoteDataSource.getShortenerResult(urlString, object : DataCallback {
            override fun onSuccess(result: Any) {
                // save in database
                runOnIoThread {
                    shortenerLocalDataSource.insertAlias(result as Alias)
                }
                callback.onSuccess(result)
            }

            override fun onFail(error: ErrorResponse) {
                callback.onFail(error)
            }

        })
    }

    fun requestFullUrl(alias: Alias, callback: DataCallback) {
        shortenerRemoteDataSource.getFullUrl(alias, object : DataCallback {
            override fun onSuccess(result: Any) {
                callback.onSuccess(result)
            }

            override fun onFail(error: ErrorResponse) {
                callback.onFail(error)
            }

        })
    }
}