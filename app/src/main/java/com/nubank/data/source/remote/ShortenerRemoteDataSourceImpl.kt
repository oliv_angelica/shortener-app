package com.nubank.data.source.remote

import com.nubank.data.Alias
import com.nubank.data.ErrorResponse
import com.nubank.data.Url
import com.nubank.data.source.DataCallback
import com.nubank.util.NetworkCheck
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ShortenerRemoteDataSourceImpl(
        private val api: ShortenerAPI,
        private val networkCheck: NetworkCheck) : ShortenerRemoteDataSource {

    override fun getShortenerResult(urlString: String, callback: DataCallback) {
        if(!networkCheck.isOnline()) {
            callback.onFail(ErrorResponse(ErrorResponse.CONNECTION_ERROR))
        } else {
            api.getAliasUrl(UrlRequest(urlString)).enqueue(object : Callback<Alias> {
                override fun onFailure(call: Call<Alias>?, t: Throwable?) {
                    callback.onFail(ErrorResponse(ErrorResponse.GENERIC_ERROR))
                }

                override fun onResponse(call: Call<Alias>?, response: Response<Alias>?) {
                    response?.body()?.let {
                        callback.onSuccess(it)
                    } ?: run {
                        callback.onFail(ErrorResponse(ErrorResponse.GENERIC_ERROR))
                    }
                }

            })
        }
    }

    override fun getFullUrl(alias: Alias, callback: DataCallback) {
        if(!networkCheck.isOnline()) {
            callback.onFail(ErrorResponse(ErrorResponse.CONNECTION_ERROR))
        } else {
            api.getFullUrl(alias = alias.alias).enqueue(object : Callback<Url> {
                override fun onFailure(call: Call<Url>?, t: Throwable?) {
                    callback.onFail(ErrorResponse(ErrorResponse.GENERIC_ERROR))
                }

                override fun onResponse(call: Call<Url>?, response: Response<Url>?) {
                    response?.body()?.let {
                        callback.onSuccess(it)
                    } ?: run {
                        callback.onFail(ErrorResponse(ErrorResponse.GENERIC_ERROR))
                    }
                }

            })
        }
    }
}