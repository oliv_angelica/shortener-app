package com.nubank.data.source

import com.nubank.data.ErrorResponse

/**
 * Callback for response in data service calls
 */
interface DataCallback {
    fun onSuccess(result: Any)

    fun onFail(error: ErrorResponse)
}