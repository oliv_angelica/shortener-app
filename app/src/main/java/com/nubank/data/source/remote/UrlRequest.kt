package com.nubank.data.source.remote

data class UrlRequest(val url: String)