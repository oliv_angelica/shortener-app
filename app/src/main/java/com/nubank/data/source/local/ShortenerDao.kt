package com.nubank.data.source.local

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.nubank.data.Alias

@Dao
interface ShortenerDao {

    @Query("SELECT * FROM alias")
    fun getAllAliases(): LiveData<List<Alias>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAlias(alias: Alias): Long
}