package com.nubank.data

import android.support.annotation.IntDef

/**
 * ErrorResponse class that only accepts error types defined below
 */
data class ErrorResponse(@ErrorType val errorCode: Int) {

    @IntDef(CONNECTION_ERROR, GENERIC_ERROR)
    @Retention(AnnotationRetention.SOURCE)
    annotation class ErrorType

    companion object {
        const val CONNECTION_ERROR = 1
        const val GENERIC_ERROR = 2
    }
}