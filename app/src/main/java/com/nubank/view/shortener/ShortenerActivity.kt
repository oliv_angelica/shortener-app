package com.nubank.view.shortener

import android.arch.lifecycle.Observer
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.nubank.R

import com.nubank.injection.Injection
import kotlinx.android.synthetic.main.activity_main.*

import android.support.v7.widget.DividerItemDecoration
import android.view.View.*
import android.app.Activity
import android.arch.lifecycle.ViewModelProvider
import android.view.inputmethod.InputMethodManager

class ShortenerActivity : AppCompatActivity() {
    private lateinit var viewModel: ShortenerViewModel
    private var listAdapter: ShortenerListAdapter? = null
    var viewModelFactory : ViewModelProvider.Factory? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        setupRecyclerViewResults()

        viewModel = Injection.provideShortenerViewModel(this, viewModelFactory)
        setupViewModelObservers()

        setupClickListeners()
    }

    private fun setupRecyclerViewResults() {
        val layoutManager = LinearLayoutManager(this)
        rvResults.layoutManager = layoutManager
        val dividerItemDecoration = DividerItemDecoration(rvResults.context,
                layoutManager.orientation)
        rvResults.addItemDecoration(dividerItemDecoration)
    }

    private fun setupViewModelObservers() {
        setupListObserver()

        setupUrlObserver()

        setupViewEventObserver()
    }

    private fun setupListObserver() {
        viewModel.aliasList.observe(this, Observer { aliasList ->
            if (listAdapter == null) {
                listAdapter = ShortenerListAdapter(viewModel)
                rvResults.adapter = listAdapter
            }
            listAdapter?.submitList(aliasList)
        })
    }

    private fun setupUrlObserver() {
        viewModel.openUrlEvent.observe(this, Observer { alias ->
            alias?.let {
                viewModel.getFullUrl(alias)
            }
        })

        viewModel.fullUrlData.observe(this, Observer { fullUrl ->
            fullUrl?.let {
                try {
                    val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(fullUrl))
                    startActivity(browserIntent)
                } catch (exception: ActivityNotFoundException) {
                    viewModel.errorOpeningUrl()
                }
            }
        })
    }

    private fun setupViewEventObserver() {
        viewModel.isLoading.observe(this, Observer { isLoading ->
            isLoading?.let {
                if (isLoading) {
                    startProgress()
                } else {
                    stopProgress()
                }
            }
        })

        viewModel.snackbarMessage.observe(this, Observer { snackbarMessage ->
            snackbarMessage?.let {
                showSnackbarMessage(snackbarMessage)
            }
        })
    }

    private fun setupClickListeners() {
        btSearch.setOnClickListener {
            hideSearchKeyboard()
            viewModel.searchUrl(etSearchUrl.text.toString())
        }
    }

    private fun hideSearchKeyboard() {
        val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(etSearchUrl.windowToken, 0)
    }

    private fun startProgress() {
        pbFetchingData.visibility = VISIBLE
        tvFetchingData.visibility = VISIBLE
    }

    private fun stopProgress() {
        pbFetchingData.visibility = GONE
        tvFetchingData.visibility = GONE
    }

    private fun showSnackbarMessage(message: Int) {
        Snackbar.make(clContainer, message, Snackbar.LENGTH_LONG)
                .show()
    }

}
