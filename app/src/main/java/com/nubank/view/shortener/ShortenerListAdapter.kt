package com.nubank.view.shortener

import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.nubank.R
import com.nubank.data.Alias

class ShortenerListAdapter(private val viewModel: ShortenerViewModel) : ListAdapter<Alias, ShortenerListAdapter.ViewHolder>(AliasDiffCallback()) {
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val itemView = LayoutInflater.from(viewGroup.context).inflate(R.layout.list_item, viewGroup, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(recyclerViewHolder: ViewHolder, position: Int) {
        val alias = getItem(position)

        updateListItem(recyclerViewHolder, alias)
    }

    private fun updateListItem(recyclerViewHolder: ViewHolder, alias: Alias) {
        recyclerViewHolder.tvAlias.text = alias.alias
        recyclerViewHolder.itemView.setOnClickListener { viewModel.openUrlEvent.value = alias }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvAlias: TextView = itemView as TextView
    }
}
