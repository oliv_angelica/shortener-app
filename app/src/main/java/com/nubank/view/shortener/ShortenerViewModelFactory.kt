package com.nubank.view.shortener

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.nubank.data.source.ShortenerRepository

/**
 * Factory for creating a [ShortenerViewModel] with a constructor that takes a [ShortenerRepository].
 */
class ShortenerViewModelFactory(
        private val repository: ShortenerRepository)
    : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>) = ShortenerViewModel(repository) as T

}