package com.nubank.view.shortener

import android.annotation.SuppressLint
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.nubank.R
import com.nubank.data.Alias
import com.nubank.data.ErrorResponse
import com.nubank.data.Url
import com.nubank.data.source.DataCallback
import com.nubank.data.source.ShortenerRepository
import com.nubank.util.isURL

class ShortenerViewModel(private val repository: ShortenerRepository) : ViewModel() {

    val aliasList : LiveData<List<Alias>> = repository.getAllAliases()
    val fullUrlData = MutableLiveData<String>()
    val openUrlEvent = MutableLiveData<Alias>()
    val snackbarMessage = MutableLiveData<Int>()
    val isLoading =  MutableLiveData<Boolean>()

    fun searchUrl(urlString: String) {
        if(urlString.isURL()) {
            isLoading.value = true

            repository.requestShortenerResult(urlString, object : DataCallback {
                override fun onSuccess(result: Any) {
                    isLoading.value = false
                    snackbarMessage.value = R.string.success_search
                }

                override fun onFail(error: ErrorResponse) {
                    isLoading.value = false
                    snackbarMessage.value = getErrorMessage(error)
                }

            })
        } else {
            snackbarMessage.value = R.string.invalid_url_error
        }
    }

    fun getFullUrl(alias: Alias) {
        isLoading.value = true

        repository.requestFullUrl(alias, object : DataCallback {
            override fun onSuccess(result: Any) {
                isLoading.value = false
                snackbarMessage.value = R.string.success_full_url
                fullUrlData.postValue((result as Url).url)
            }

            override fun onFail(error: ErrorResponse) {
                isLoading.value = false
                snackbarMessage.value = getErrorMessage(error)
            }

        })
    }

    fun errorOpeningUrl() {
        snackbarMessage.value = R.string.error_opening_url
    }

    @SuppressLint("SwitchIntDef")
    private fun getErrorMessage(errorResponse: ErrorResponse) : Int {
        return when(errorResponse.errorCode) {
            ErrorResponse.CONNECTION_ERROR -> {
                R.string.internet_error_message
            }
            else -> R.string.error_message
        }
    }
}