package com.nubank.view.shortener

import android.support.v7.util.DiffUtil
import com.nubank.data.Alias

class AliasDiffCallback : DiffUtil.ItemCallback<Alias>() {
    override fun areItemsTheSame(oldItem: Alias, newItem: Alias) = oldItem.alias.equals(newItem.alias)

    override fun areContentsTheSame(oldItem: Alias, newItem: Alias) = oldItem.alias.equals(newItem.alias)
}