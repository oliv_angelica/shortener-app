package com.nubank.view.shortener

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.nubank.R
import com.nubank.data.source.DataCallback
import com.nubank.data.source.ShortenerRepository
import com.nubank.util.Constants.ALIAS_TEST
import com.nubank.util.Constants.ERROR_RESPONSE_CONNECTION
import com.nubank.util.Constants.ERROR_RESPONSE_GENERIC
import com.nubank.util.Constants.INVALID_URL
import com.nubank.util.Constants.URL_TEST
import com.nubank.util.any
import com.nubank.util.capture
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import java.lang.Boolean.FALSE
import java.lang.Boolean.TRUE

class ShortenerViewModelTest {

    @get:Rule
    var instantExecutorRule =  InstantTaskExecutorRule()
    private lateinit var shortenerViewModel: ShortenerViewModel
    @Mock private lateinit var shortenerRepository: ShortenerRepository
    @Captor private lateinit var callbackCaptor: ArgumentCaptor<DataCallback>

    @Before
    fun setupShortenerViewModel() {
        MockitoAnnotations.initMocks(this)

        shortenerViewModel = ShortenerViewModel(shortenerRepository)

    }

    @Test
    fun searchUrl_invalidUrlInSnackbarMessage() {
        // Action
        shortenerViewModel.searchUrl(INVALID_URL)

        // Assertion
        assertEquals(R.string.invalid_url_error, shortenerViewModel.snackbarMessage.value)
    }

    @Test
    fun searchUrl_loadingIsShown() {
        // Action
        shortenerViewModel.searchUrl(URL_TEST.url)

        // Assertion
        assertEquals(TRUE, shortenerViewModel.isLoading.value)
    }

    @Test
    fun searchUrl_successResponse() {
        // Action
        shortenerViewModel.searchUrl(URL_TEST.url)

        // Mock
        verify(shortenerRepository).requestShortenerResult(anyString(), capture(callbackCaptor))
        callbackCaptor.value.onSuccess(ALIAS_TEST)

        // Assertion
        assertEquals(FALSE, shortenerViewModel.isLoading.value)
        assertEquals(R.string.success_search, shortenerViewModel.snackbarMessage.value)
    }

    @Test
    fun searchUrl_failureResponseGeneric() {
        // Action
        shortenerViewModel.searchUrl(URL_TEST.url)

        // Mock
        verify(shortenerRepository).requestShortenerResult(anyString(), capture(callbackCaptor))
        callbackCaptor.value.onFail(ERROR_RESPONSE_GENERIC)

        // Assertion
        assertEquals(FALSE, shortenerViewModel.isLoading.value)
        assertEquals(R.string.error_message, shortenerViewModel.snackbarMessage.value)
    }

    @Test
    fun searchUrl_failureResponseConnection() {
        // Action
        shortenerViewModel.searchUrl(URL_TEST.url)

        // Mock
        verify(shortenerRepository).requestShortenerResult(anyString(), capture(callbackCaptor))
        callbackCaptor.value.onFail(ERROR_RESPONSE_CONNECTION)

        // Assertion
        assertEquals(FALSE, shortenerViewModel.isLoading.value)
        assertEquals(R.string.internet_error_message, shortenerViewModel.snackbarMessage.value)
    }

    @Test
    fun getFullUrl_loadingIsShown() {
        // Action
        shortenerViewModel.getFullUrl(ALIAS_TEST)

        // Assertion
        assertEquals(TRUE, shortenerViewModel.isLoading.value)
    }

    @Test
    fun getFullUrl_successResponse() {
        // Action
        shortenerViewModel.getFullUrl(ALIAS_TEST)

        // Mock
        verify(shortenerRepository).requestFullUrl(any(), capture(callbackCaptor))
        callbackCaptor.value.onSuccess(URL_TEST)

        // Assertion
        assertEquals(FALSE, shortenerViewModel.isLoading.value)
        assertEquals(R.string.success_full_url, shortenerViewModel.snackbarMessage.value)
        assertEquals(URL_TEST.url, shortenerViewModel.fullUrlData.value)
    }

    @Test
    fun getFullUrl_failureResponseGeneric() {
        // Action
        shortenerViewModel.getFullUrl(ALIAS_TEST)

        // Mock
        verify(shortenerRepository).requestFullUrl(any(), capture(callbackCaptor))
        callbackCaptor.value.onFail(ERROR_RESPONSE_GENERIC)

        // Assertion
        assertEquals(FALSE, shortenerViewModel.isLoading.value)
        assertEquals(R.string.error_message, shortenerViewModel.snackbarMessage.value)
    }

    @Test
    fun getFullUrl_failureResponseConnection() {
        // Action
        shortenerViewModel.getFullUrl(ALIAS_TEST)

        // Mock
        verify(shortenerRepository).requestFullUrl(any(), capture(callbackCaptor))
        callbackCaptor.value.onFail(ERROR_RESPONSE_CONNECTION)

        // Assertion
        assertEquals(FALSE, shortenerViewModel.isLoading.value)
        assertEquals(R.string.internet_error_message, shortenerViewModel.snackbarMessage.value)
    }
}