package com.nubank.data.source.remote

import com.nubank.data.Alias
import com.nubank.data.Url
import com.nubank.data.source.DataCallback
import com.nubank.util.Constants.ALIAS_TEST
import com.nubank.util.Constants.ERROR_CODE_REMOTE
import com.nubank.util.Constants.ERROR_RESPONSE_CONNECTION
import com.nubank.util.Constants.ERROR_RESPONSE_GENERIC
import com.nubank.util.Constants.URL_TEST
import com.nubank.util.NetworkCheck
import com.nubank.util.capture
import com.nubank.util.mock
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Boolean.FALSE
import java.lang.Boolean.TRUE

class ShortenerRemoteDataSourceImplTest {

    private lateinit var shortenerRemoteDataSource: ShortenerRemoteDataSourceImpl
    @Mock private lateinit var api: ShortenerAPI
    @Mock private lateinit var networkCheck: NetworkCheck
    @Mock private lateinit var callback: DataCallback
    @Mock private lateinit var callAlias: Call<Alias>
    @Captor private lateinit var callbackCaptorAlias: ArgumentCaptor<Callback<Alias>>
    @Mock private lateinit var callUrl: Call<Url>
    @Captor private lateinit var callbackCaptorUrl: ArgumentCaptor<Callback<Url>>

    @Before
    fun setupShortenerRemoteDataSource() {
        MockitoAnnotations.initMocks(this)

        shortenerRemoteDataSource = ShortenerRemoteDataSourceImpl(api, networkCheck)

        // Mock getAlias return
        `when`(api.getAliasUrl(UrlRequest(URL_TEST.url))).thenReturn(callAlias)

        // Mock getFullUrl return
        `when`(api.getFullUrl(ALIAS_TEST.alias)).thenReturn(callUrl)
    }

    @Test
    fun getShortenerResult_noInternetConnection() {
        // Mock
        `when`(networkCheck.isOnline()).thenReturn(FALSE)

        // Action
        shortenerRemoteDataSource.getShortenerResult(URL_TEST.url, callback)

        // Assertion
        verify(callback).onFail(ERROR_RESPONSE_CONNECTION)
    }

    @Test
    fun getShortenerResult_successResponse() {
        // Mock
        `when`(networkCheck.isOnline()).thenReturn(TRUE)

        // Action
        shortenerRemoteDataSource.getShortenerResult(URL_TEST.url, callback)

        // Mock
        verify(callAlias).enqueue(capture(callbackCaptorAlias))
        val successResponse =  Response.success(ALIAS_TEST)
        callbackCaptorAlias.value.onResponse(callAlias, successResponse)

        // Assertion
        verify(callback).onSuccess(ALIAS_TEST)
    }

    @Test
    fun getShortenerResult_failureResponse() {
        // Mock
        `when`(networkCheck.isOnline()).thenReturn(TRUE)

        // Action
        shortenerRemoteDataSource.getShortenerResult(URL_TEST.url, callback)

        // Mock
        verify(callAlias).enqueue(capture(callbackCaptorAlias))
        val errorResponse =  Response.error<Alias>(ERROR_CODE_REMOTE, mock())
        callbackCaptorAlias.value.onResponse(callAlias, errorResponse)

        // Assertion
        verify(callback).onFail(ERROR_RESPONSE_GENERIC)
    }

    @Test
    fun getFullUrl_noInternetConnection() {
        // Mock
        `when`(networkCheck.isOnline()).thenReturn(FALSE)

        // Action
        shortenerRemoteDataSource.getFullUrl(ALIAS_TEST, callback)

        // Assertion
        verify(callback).onFail(ERROR_RESPONSE_CONNECTION)
    }

    @Test
    fun getFullUrl_successResponse() {
        // Mock
        `when`(networkCheck.isOnline()).thenReturn(TRUE)

        // Action
        shortenerRemoteDataSource.getFullUrl(ALIAS_TEST, callback)

        // Mock
        verify(callUrl).enqueue(capture(callbackCaptorUrl))
        val successResponse =  Response.success(URL_TEST)
        callbackCaptorUrl.value.onResponse(callUrl, successResponse)

        // Assertion
        verify(callback).onSuccess(URL_TEST)
    }

    @Test
    fun getFullUrl_failureResponse() {
        // Mock
        `when`(networkCheck.isOnline()).thenReturn(TRUE)

        // Action
        shortenerRemoteDataSource.getFullUrl(ALIAS_TEST, callback)

        // Mock
        verify(callUrl).enqueue(capture(callbackCaptorUrl))
        val errorResponse =  Response.error<Url>(ERROR_CODE_REMOTE, mock())
        callbackCaptorUrl.value.onResponse(callUrl, errorResponse)

        // Assertion
        verify(callback).onFail(ERROR_RESPONSE_GENERIC)
    }
}