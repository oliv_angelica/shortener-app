package com.nubank.data.source

import com.nubank.data.Alias
import com.nubank.data.ErrorResponse
import com.nubank.data.Url
import com.nubank.data.source.local.ShortenerDao
import com.nubank.data.source.remote.ShortenerRemoteDataSourceImpl
import com.nubank.util.any
import com.nubank.util.capture
import org.junit.Before
import org.junit.Test
import org.mockito.*
import org.mockito.Mockito.*


class ShortenerRepositoryTest {

    private lateinit var shortenerRepository: ShortenerRepository
    @Mock private lateinit var shortenerRemoteDataSource: ShortenerRemoteDataSourceImpl
    @Mock private lateinit var shortenerDao: ShortenerDao
    @Mock private lateinit var callback: DataCallback
    @Captor private lateinit var callbackCaptor: ArgumentCaptor<DataCallback>

    @Before
    fun setupShortenerRepository() {
        MockitoAnnotations.initMocks(this)

        shortenerRepository = ShortenerRepository(shortenerRemoteDataSource, shortenerDao)
    }

    @Test
    fun requestShortenerResult_onSuccess() {
        // Action
        shortenerRepository.requestShortenerResult(VALID_URL, callback)

        // Mock
        verify(shortenerRemoteDataSource).getShortenerResult(anyString(), capture(callbackCaptor))
        callbackCaptor.value.onSuccess(ALIAS)

        // Assertion
        verify(callback).onSuccess(ALIAS)
    }

    @Test
    fun requestShortenerResult_onFailGenericError() {
        // Action
        shortenerRepository.requestShortenerResult(VALID_URL, callback)

        // Mock
        verify(shortenerRemoteDataSource).getShortenerResult(anyString(), capture(callbackCaptor))
        callbackCaptor.value.onFail(GENERIC_ERROR)

        // Assertion
        verify(callback).onFail(GENERIC_ERROR)
    }

    @Test
    fun requestShortenerResult_onFailConnectionError() {
        // Action
        shortenerRepository.requestShortenerResult(VALID_URL, callback)

        // Mock
        verify(shortenerRemoteDataSource).getShortenerResult(anyString(), capture(callbackCaptor))
        callbackCaptor.value.onFail(CONNECTION_ERROR)

        // Assertion
        verify(callback).onFail(CONNECTION_ERROR)
    }

    @Test
    fun requestFullUrl_onSuccess() {
        // Action
        shortenerRepository.requestFullUrl(ALIAS, callback)

        // Mock
        verify(shortenerRemoteDataSource).getFullUrl(any(), capture(callbackCaptor))
        callbackCaptor.value.onSuccess(URL)

        // Assertion
        verify(callback).onSuccess(URL)
    }

    @Test
    fun requestFullUrl_onFailGenericError() {
        // Action
        shortenerRepository.requestFullUrl(ALIAS, callback)

        // Mock
        verify(shortenerRemoteDataSource).getFullUrl(any(), capture(callbackCaptor))
        callbackCaptor.value.onFail(GENERIC_ERROR)

        // Assertion
        verify(callback).onFail(GENERIC_ERROR)
    }

    @Test
    fun requestFullUrl_onFailConnectionError() {
        // Action
        shortenerRepository.requestFullUrl(ALIAS, callback)

        // Mock
        verify(shortenerRemoteDataSource).getFullUrl(any(), capture(callbackCaptor))
        callbackCaptor.value.onFail(CONNECTION_ERROR)

        // Assertion
        verify(callback).onFail(CONNECTION_ERROR)
    }

    companion object {
        private const val VALID_URL = "https://nubank.com.br"
        private val ALIAS = Alias("57044")
        private val URL = Url(VALID_URL)
        private val GENERIC_ERROR = ErrorResponse(ErrorResponse.GENERIC_ERROR)
        private val CONNECTION_ERROR = ErrorResponse(ErrorResponse.CONNECTION_ERROR)
    }
}