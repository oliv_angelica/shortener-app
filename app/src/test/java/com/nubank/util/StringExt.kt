package com.nubank.util

fun String.isURL() = !this.equals(Constants.INVALID_URL)
