package com.nubank.util

import com.nubank.data.Alias
import com.nubank.data.ErrorResponse
import com.nubank.data.Url

object Constants {
    val URL_TEST = Url("https://nubank.com.br")

    val ALIAS_TEST = Alias("48155")

    const val INVALID_URL = "nubank"

    val ERROR_RESPONSE_GENERIC = ErrorResponse(ErrorResponse.GENERIC_ERROR)

    val ERROR_RESPONSE_CONNECTION = ErrorResponse(ErrorResponse.CONNECTION_ERROR)

    const val ERROR_CODE_REMOTE = 500
}