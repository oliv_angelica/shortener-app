package com.nubank.injection

import android.content.Context
import android.support.v7.app.AppCompatActivity
import com.nubank.data.source.ShortenerRepository
import com.nubank.data.source.local.AppDatabase
import com.nubank.data.source.local.ShortenerDao
import com.nubank.data.source.remote.ShortenerRemoteDataSourceFakeImpl
import com.nubank.view.shortener.ShortenerViewModelFactory

/**
 * Injection class to override behaviour from RemoteDataSource
 */
object Injection {
    fun provideShortenerViewModelFactory(activity: AppCompatActivity) = ShortenerViewModelFactory(provideSortenerRepository(activity))

    fun provideSortenerRepository(context: Context) = ShortenerRepository(provideRemoteDataSource(),
            provideShortenerDao(context))

    fun provideRemoteDataSource() = ShortenerRemoteDataSourceFakeImpl()

    fun provideShortenerDao(context: Context) : ShortenerDao =
            AppDatabase.getInstance(context)
                    .shortenerDao()
}