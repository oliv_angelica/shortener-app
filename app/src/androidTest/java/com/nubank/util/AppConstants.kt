package com.nubank.util

import com.nubank.data.Alias
import com.nubank.data.Url

object AppConstants {

    val URL_TEST = Url("https://nubank.com.br")

    val ALIAS_TEST = Alias("48155")

    const val INVALID_URL = "nubank"
}