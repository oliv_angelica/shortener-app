package com.nubank.data.source.remote

import com.nubank.data.Alias
import com.nubank.data.source.DataCallback
import com.nubank.util.AppConstants.ALIAS_TEST
import com.nubank.util.AppConstants.URL_TEST

/**
 * Fake implementation for ShortenerRemoteDataSource just for Android Screen Test
 */
class ShortenerRemoteDataSourceFakeImpl : ShortenerRemoteDataSource{
    override fun getShortenerResult(urlString: String, callback: DataCallback) {
        callback.onSuccess(ALIAS_TEST)
    }

    override fun getFullUrl(alias: Alias, callback: DataCallback) {
        callback.onSuccess(URL_TEST)
    }
}