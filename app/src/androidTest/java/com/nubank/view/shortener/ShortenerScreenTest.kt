package com.nubank.view.shortener

import android.content.Intent
import android.net.Uri
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.typeText
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.intent.Intents.intended
import android.support.test.espresso.intent.matcher.IntentMatchers.hasAction
import android.support.test.espresso.intent.matcher.IntentMatchers.hasData
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.runner.AndroidJUnit4
import com.nubank.R
import com.nubank.util.AppConstants.ALIAS_TEST
import com.nubank.util.AppConstants.INVALID_URL
import com.nubank.util.AppConstants.URL_TEST
import org.hamcrest.CoreMatchers.allOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class) class ShortenerScreenTest {
    @Rule
    @JvmField var shortenerRule =
            IntentsTestRule(ShortenerActivity::class.java)

    @Test
    fun searchUrl_validAliasIsShown() {
        // search for a valid URL
        addValidURL()

        // Check if alias is displayed
        onView(withText(ALIAS_TEST.alias))
                .check(matches(isDisplayed()))
    }

    @Test
    fun searchInvalidUrl_errorMessageIsShown() {
        // Type a invalid URL
        onView(withId(R.id.etSearchUrl))
                .perform(typeText(INVALID_URL))

        // Click on search button
        onView(withId(R.id.btSearch))
                .perform(click())

        // Check if error message is displayed
        onView(withText(R.string.invalid_url_error))
                .check(matches(isDisplayed()))
    }

    @Test
    fun searchEmptyUrl_errorMessageIsShown() {
        // Click on search button (without type anything)
        onView(withId(R.id.btSearch))
                .perform(click())

        // Check if error message is displayed
        onView(withText(R.string.invalid_url_error))
                .check(matches(isDisplayed()))
    }

    @Test
    fun clickOnAlias_browserIsOpened() {
        // search for a valid URL
        addValidURL()

        // click on Alias
        onView(withText(ALIAS_TEST.alias))
                .perform(click())

        // check intent is started
        intended(allOf(
                hasAction(Intent.ACTION_VIEW),
                hasData(Uri.parse(URL_TEST.url))))
    }

    private fun addValidURL() {
        // Type a valid URL
        onView(withId(R.id.etSearchUrl))
                .perform(typeText(URL_TEST.url))

        // Click on search button
        onView(withId(R.id.btSearch))
                .perform(click())
    }
}