package com.nubank.view.shortener

import android.support.test.rule.ActivityTestRule
import android.support.v7.app.AppCompatActivity
import com.nubank.injection.Injection

class ShortenerActivityRule(activityClass: Class<ShortenerActivity>,
                            initialTouchMode: Boolean,
                            launchActivity: Boolean) :
        ActivityTestRule<ShortenerActivity>(activityClass, initialTouchMode, launchActivity) {

    override fun beforeActivityLaunched() {
        super.beforeActivityLaunched()
//        activity.run {
//            viewModelFactory = Injection.provideShortenerViewModelFactory(this)
//        }
    }
}